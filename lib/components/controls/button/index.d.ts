import * as React from 'react';
import { ButtonProps } from './types';
export declare class Button extends React.Component<ButtonProps> {
    render(): JSX.Element;
}

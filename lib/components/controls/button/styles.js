"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var no_important_1 = require("aphrodite/no-important");
var style_helper_1 = require("./../../../helpers/style-helper");
exports.styles = no_important_1.StyleSheet.create({
    button: style_helper_1.createStyle(__assign({}, style_helper_1.paddingMixed(11, 40), { fontSize: 16, borderRadius: 26 }))
});
//# sourceMappingURL=styles.js.map
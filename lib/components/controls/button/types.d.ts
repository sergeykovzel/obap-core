import { ButtonHTMLAttributes } from 'react';
import { StyleType } from './../../../types/core/style-type';
interface ButtonOwnProps {
    styles?: StyleType;
}
export declare type ButtonProps = ButtonOwnProps & ButtonHTMLAttributes<{}>;
export {};

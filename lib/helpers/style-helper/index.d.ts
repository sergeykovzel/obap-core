import { CSSProperties } from 'aphrodite/no-important';
export * from './padding';
export declare const createStyle: (properties: CSSProperties) => CSSProperties;

"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.paddingHorizontal = function (paddingLeft, paddingRight) {
    if (paddingRight === void 0) { paddingRight = paddingLeft; }
    return ({
        paddingLeft: paddingLeft,
        paddingRight: paddingRight,
    });
};
exports.paddingVertical = function (paddingTop, paddingBottom) {
    if (paddingBottom === void 0) { paddingBottom = paddingTop; }
    return ({
        paddingTop: paddingTop,
        paddingBottom: paddingBottom,
    });
};
exports.paddingMixed = function (verticalValue, horizontalValue) {
    if (horizontalValue === void 0) { horizontalValue = verticalValue; }
    return (__assign({}, exports.paddingVertical(verticalValue), exports.paddingHorizontal(horizontalValue)));
};
exports.padding = function (paddingTop, paddingRight, paddingBottom, paddingLeft) { return ({
    paddingTop: paddingTop,
    paddingRight: paddingRight,
    paddingBottom: paddingBottom,
    paddingLeft: paddingLeft,
}); };
//# sourceMappingURL=padding.js.map
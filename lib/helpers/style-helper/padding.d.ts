import { CSSProperties } from 'aphrodite/no-important';
export declare const paddingHorizontal: (paddingLeft: string | number, paddingRight?: string | number) => CSSProperties;
export declare const paddingVertical: (paddingTop: string | number, paddingBottom?: string | number) => CSSProperties;
export declare const paddingMixed: (verticalValue: string | number, horizontalValue?: string | number) => CSSProperties;
export declare const padding: (paddingTop: string | number, paddingRight: string | number, paddingBottom: string | number, paddingLeft: string | number) => CSSProperties;

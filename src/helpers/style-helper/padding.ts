import { CSSProperties } from 'aphrodite/no-important'

export const paddingHorizontal = (
  paddingLeft: number | string,
  paddingRight: number | string = paddingLeft,
): CSSProperties => ({
  paddingLeft,
  paddingRight,
})

export const paddingVertical = (
  paddingTop: number | string,
  paddingBottom: number | string = paddingTop,
): CSSProperties => ({
  paddingTop,
  paddingBottom,
})

export const paddingMixed = (
  verticalValue: number | string,
  horizontalValue: number | string = verticalValue,
): CSSProperties => ({
  ...paddingVertical(verticalValue),
  ...paddingHorizontal(horizontalValue),
})

export const padding = (
  paddingTop: number | string,
  paddingRight: number | string,
  paddingBottom: number | string,
  paddingLeft: number | string,
): CSSProperties => ({
  paddingTop,
  paddingRight,
  paddingBottom,
  paddingLeft,
})

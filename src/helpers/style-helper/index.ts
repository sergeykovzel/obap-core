import { CSSProperties } from 'aphrodite/no-important'

export * from './padding'

export const createStyle = (properties: CSSProperties): CSSProperties => properties

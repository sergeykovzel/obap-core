import { StyleDeclarationValue } from 'aphrodite/no-important'

export type StyleType = StyleDeclarationValue | StyleDeclarationValue[]

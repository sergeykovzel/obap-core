import * as React from 'react'
import { css } from 'aphrodite/no-important'

import { ButtonProps } from './types'
import { styles } from './styles'

export class Button extends React.Component<ButtonProps> {
  public render(): JSX.Element {
    const {
      styles: buttonStyles,
      ...restProps
    } = this.props
    return (
      <button
        className={css(styles.button, buttonStyles)}
        {...restProps}
      />
    )
  }
}

import { StyleSheet } from 'aphrodite/no-important'
import {
  createStyle,
  paddingMixed,
} from './../../../helpers/style-helper'

export const styles = StyleSheet.create({
  button: createStyle({
    ...paddingMixed(11, 40),
    fontSize: 16,
    borderRadius: 26,
  })
})

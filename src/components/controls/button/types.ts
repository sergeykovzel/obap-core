import { ButtonHTMLAttributes } from 'react'
import { StyleType } from './../../../types/core/style-type'

interface ButtonOwnProps {
  styles?: StyleType
}

export type ButtonProps =
  ButtonOwnProps &
  ButtonHTMLAttributes<{}>
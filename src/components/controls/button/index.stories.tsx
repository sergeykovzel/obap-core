import React from 'react';
import { storiesOf } from '@storybook/react';

import { Button } from './index';

storiesOf('Controls', module)
  .add('Button', () => <Button>123</Button>);
